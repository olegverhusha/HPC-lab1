#!/bin/bash
#PBS -l walltime=00:02:00
#PBS -l nodes=1:ppn=1
#PBS -N Verhusha_integral
#PBS -t 1-22

cd $PBS_O_WORKDIR

input_file="input_data.txt"
output_file="solvOut_${PBS_ARRAYID}.out"

if [ ! -f "$input_file" ]; then
    echo "Input file '$input_file' not found."
    exit 1
fi

line=$(sed -n "${PBS_ARRAYID}p" "$input_file")
read -r xmin xmax <<< "$line"

result=$(./integral "$xmin" "$xmax")

if [ $? -ne 0 ]; then
    echo "Error occurred while running the program."
    exit 1
fi

echo "$result" > "$output_file"