#!/bin/bash

xmin=-114790
xmax=59170
steps=220

awk -v xmin="$xmin" -v xmax="$xmax" -v steps="$steps" 'BEGIN {
    dx = (xmax - xmin) / steps;
    x = xmin;
    for (i = 1; i <= steps; i++) {
        print x, x + dx;
        x += dx;
    }
}' > solvM.inp
