#!/bin/bash

xmin=-11479
xmax=5917
steps=22

awk -v xmin="$xmin" -v xmax="$xmax" -v steps="$steps" 'BEGIN {
    dx = (xmax - xmin) / steps;
    x = xmin;
    for (i = 1; i <= steps; i++) {
        print x, x + dx;
        x += dx;
    }
}' > input_data.txt
