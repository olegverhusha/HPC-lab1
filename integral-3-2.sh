#!/bin/bash
#PBS -l walltime=00:02:00
#PBS -l nodes=1:ppn=1
#PBS -N Verhusha_integral
cd $PBS_O_WORKDIR

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_file> <output_file>"
    exit 1
fi

input_file=$1
output_file=$2

read xmin xmax < "$input_file"

result=$(./integral "$xmin" "$xmax")

echo "$result" > "$output_file"
