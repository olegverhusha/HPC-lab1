#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_file> <output_file>"
    exit 1
fi

input_file=$1
output_file=$2

read xmin xmax < "$input_file"

result=$(./integral "$xmin" "$xmax")

echo "$result" > "$output_file"
